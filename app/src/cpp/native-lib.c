/*

The MIT License (MIT)

Copyright (c) 2018  Dmitrii Kozhevin <kozhevin.dima@gmail.com>

Permission is hereby granted, free of charge, to any person obtaining a copy of this software
and associated documentation files (the “Software”), to deal in the Software without
restriction, including without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or
substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 */

#include <jni.h>
#include <malloc.h>
#include <string.h>
#include <sys/time.h>
#include "path_helper.h"
#include "unzip_helper.h"
#include "pkcs7_helper.h"
#include "log.h"
#include "comulez_github_encryptlib_Encrypt4C.h"
#include <stdio.h>
#include <stdint.h>

#define CBC 1
#define ECB 1


static void test_encrypt_ecb(void);

jbyteArray as_byte_array(JNIEnv *env, unsigned char *buf, int len);

unsigned char *as_unsigned_char_array(JNIEnv *env, jbyteArray array);

#include "aes.h"


const char *getSignaturex(JNIEnv *env, jobject context) {
    // Build.VERSION.SDK_INT
    jclass versionClass = (*env)->FindClass(env, "android/os/Build$VERSION");
    jfieldID sdkIntFieldID = (*env)->GetStaticFieldID(env, versionClass, "SDK_INT", "I");
    int sdkInt = (*env)->GetStaticIntField(env, versionClass, sdkIntFieldID);
    // Context
    jclass contextClass = (*env)->FindClass(env, "android/content/Context");
    // Context#getPackageManager()
    jmethodID pmMethod = (*env)->GetMethodID(env, contextClass, "getPackageManager", "()Landroid/content/pm/PackageManager;");
    jobject pm = (*env)->CallObjectMethod(env, context, pmMethod);
    jclass pmClass = (*env)->GetObjectClass(env, pm);
    // PackageManager#getPackageInfo()
    jmethodID piMethod = (*env)->GetMethodID(env, pmClass, "getPackageInfo", "(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;");
    // Context#getPackageName()
    jmethodID pnMethod = (*env)->GetMethodID(env, contextClass, "getPackageName", "()Ljava/lang/String;");
    jstring packageName = (jstring) ((*env)->CallObjectMethod(env, context, pnMethod));
    int flags;
    if (sdkInt >= 28) {
        flags = 0x08000000; // PackageManager.GET_SIGNING_CERTIFICATES
    } else {
        flags = 0x00000040; // PackageManager.GET_SIGNATURES
    }
    jobject packageInfo = (*env)->CallObjectMethod(env, pm, piMethod, packageName, flags);
    jclass piClass = (*env)->GetObjectClass(env, packageInfo);
    // PackageInfo#signingInfo.apkContentsSigners | PackageInfo#signatures
    jobjectArray signatures;
    if (sdkInt >= 28) {
        // PackageInfo#signingInfo
        jfieldID signingInfoField = (*env)->GetFieldID(env, piClass, "signingInfo", "Landroid/content/pm/SigningInfo;");
        jobject signingInfoObject = (*env)->GetObjectField(env, packageInfo, signingInfoField);
        jclass signingInfoClass = (*env)->GetObjectClass(env, signingInfoObject);
        // SigningInfo#apkContentsSigners
        jmethodID signaturesMethod = (*env)->GetMethodID(env, signingInfoClass, "getApkContentsSigners", "()[Landroid/content/pm/Signature;");
        jobject signaturesObject = (*env)->CallObjectMethod(env, signingInfoObject, signaturesMethod);
        signatures = (jobjectArray) (signaturesObject);
    } else {
        // PackageInfo#signatures
        jfieldID signaturesField = (*env)->GetFieldID(env, piClass, "signatures", "[Landroid/content/pm/Signature;");
        jobject signaturesObject = (*env)->GetObjectField(env, packageInfo, signaturesField);
        if ((*env)->IsSameObject(env, signaturesObject, NULL)) {
            return ""; // in case signatures is null
        }
        signatures = (jobjectArray) (signaturesObject);
    }
    // Signature[0]
    jobject firstSignature = (*env)->GetObjectArrayElement(env, signatures, 0);
    jclass signatureClass = (*env)->GetObjectClass(env, firstSignature);
    // PackageInfo#signatures[0].toCharString()
    jmethodID signatureByteMethod = (*env)->GetMethodID(env, signatureClass, "toByteArray", "()[B");
    jobject signatureByteArray = (jobject) (*env)->CallObjectMethod(env, firstSignature, signatureByteMethod);
    // MessageDigest.getInstance("MD5")
    jclass mdClass = (*env)->FindClass(env, "java/security/MessageDigest");
    jmethodID mdMethod = (*env)->GetStaticMethodID(env, mdClass, "getInstance", "(Ljava/lang/String;)Ljava/security/MessageDigest;");
    jobject md5Object = (*env)->CallStaticObjectMethod(env, mdClass, mdMethod, (*env)->NewStringUTF(env, "MD5"));
    // MessageDigest#update
    jmethodID mdUpdateMethod = (*env)->GetMethodID(env, mdClass, "update", "([B)V");// The return value of this function is void, write V
    (*env)->CallVoidMethod(env, md5Object, mdUpdateMethod, signatureByteArray);
    // MessageDigest#digest
    jmethodID mdDigestMethod = (*env)->GetMethodID(env, mdClass, "digest", "()[B");
    jobject fingerPrintByteArray = (*env)->CallObjectMethod(env, md5Object, mdDigestMethod);
    // iterate over the bytes and convert to md5 array
    jsize byteArrayLength = (*env)->GetArrayLength(env, fingerPrintByteArray);
    jbyte *fingerPrintByteArrayElements = (*env)->GetByteArrayElements(env, fingerPrintByteArray, JNI_FALSE);
    char *charArray = (char *) fingerPrintByteArrayElements;
    char *md5 = (char *) calloc(2 * byteArrayLength + 1, sizeof(char));
    int k;
    for (k = 0; k < byteArrayLength; k++) {
        sprintf(&md5[2 * k], "%02X", charArray[k]); // Not sure if the cast is needed
    }
    return md5;
}

jstring genString(uint64_t val)
{
    char temp[21];
    sprintf(temp, "%z", val);
    return temp;
}

jstring getTimeNsec() {
    struct timespec now;
    clock_gettime(CLOCK_MONOTONIC, &now);
    return genString((int64_t) now.tv_sec*1000000000LL + now.tv_nsec);
}

long long currentTimeInMilliseconds()
{
    struct timeval tv;
    gettimeofday(&tv, NULL);
    return ((tv.tv_sec * 1000) + (tv.tv_usec / 1000));
}

JNIEXPORT jbyteArray JNICALL
Java_com_fridaystu_thaicook_feature_main_MainActivity_bytesFromJNI(JNIEnv *env, jobject context) {
    const char * signaturex  = getSignaturex(env, context);
    char destination[] = "jumpz_";
    long long x = currentTimeInMilliseconds();
    char timestamp[256];
    sprintf(timestamp, "_%lld", x);
    char *concatenated;
    concatenated = malloc(strlen(destination) + strlen(signaturex) + strlen(timestamp) + 1);
    strcpy(concatenated, destination);
    strcat(concatenated, signaturex);
    strcat(concatenated, timestamp);

    char secret[] = ",jumpz_dawdwdwa23123123dqwewsddd";
    strcat(concatenated, secret);
    NSV_LOGI("pathHelperGetPath starts [%s] \n", concatenated);


    if (strcmp("66C60BDAE163ECDB2A8871C0B53FFF00", signaturex) == 0) {
        // define the logic to retrieve or compute the secret
        return (*env)->NewStringUTF(env, concatenated);
    } else {
        return (*env)->NewStringUTF(env, "dwad awdawd2342342343423 , awdwddw");
    }



//    jsize alen = strlen(concatenated);
//    unsigned char *originChar ;
//    originChar = *(unsigned char*)(&concatenated);
//
//    char keytxt[] = "mxId8oQx7M6cFHtxuRHiHFx4M1211Csd";
//    unsigned char *keyByteChar;
//    keyByteChar = *(unsigned char*)(&keytxt);
//
//    //originChar 正确；
//    int len;
//    if (alen % 16 == 0) {
//        int xxx = alen / 16;
//        len = (xxx) * 16;
//    } else {
//        int xxx = alen / 16;
//        len = (xxx + 1) * 16;
//    }
//    int ss = sizeof(originChar);
//    uint8_t buffer[len];
//    AES_ECB_encrypt(originChar, keyByteChar, buffer, len);
//    return as_byte_array(env, buffer, len);

//    NSV_LOGI("pathHelperGetPath starts\n");
//    char *path = pathHelperGetPath();
//    NSV_LOGI("pathHelperGetPath finishes\n");
//
//    if (!path) {
//        return NULL;
//    }
//    NSV_LOGI("pathHelperGetPath result[%s]\n", path);
//    NSV_LOGI("unzipHelperGetCertificateDetails starts\n");
//    size_t len_in = 0;
//    size_t len_out = 0;
//    unsigned char *content = unzipHelperGetCertificateDetails(path, &len_in);
//    NSV_LOGI("unzipHelperGetCertificateDetails finishes\n");
//    if (!content) {
//        free(path);
//        return NULL;
//    }
//    NSV_LOGI("pkcs7HelperGetSignature starts\n");
//
//    unsigned char *res = pkcs7HelperGetSignature(content, len_in, &len_out);
//    NSV_LOGI("pkcs7HelperGetSignature finishes\n");
//    jbyteArray jbArray = NULL;
//    if (NULL != res || len_out != 0) {
//        jbArray = (*env)->NewByteArray(env, len_out);
//        (*env)->SetByteArrayRegion(env, jbArray, 0, len_out, (jbyte *) res);
//    }
//    free(content);
//    free(path);
//    pkcs7HelperFree();
    jbyteArray jbArray = NULL;
    return jbArray;
}


/**
 * 加密
 * @param env
 * @param instance
 * @param originByte
 * @param keyByte
 * @return
 */

JNIEXPORT jbyteArray JNICALL
Java_com_fridaystu_thaicook_feature_main_MainActivity_AES_1ECB_1encrypt_1byte
        (JNIEnv *env, jobject instance, jbyteArray originByte, jbyteArray keyByte) {
    jsize alen = (*env)->GetArrayLength(env, originByte); //获取长度
    unsigned char *originChar = as_unsigned_char_array(env, originByte);
    unsigned char *keyByteChar = as_unsigned_char_array(env, keyByte);
    //originChar 正确；
    int len;
    if (alen % 16 == 0) {
        int xxx = alen / 16;
        len = (xxx) * 16;
    } else {
        int xxx = alen / 16;
        len = (xxx + 1) * 16;
    }
    int ss = sizeof(originChar);
    uint8_t buffer[len];
    AES_ECB_encrypt(originChar, keyByteChar, buffer, len);
    return as_byte_array(env, buffer, len);
}

/**
 * 解密
 * @param env
 * @param instance
 * @param originByte
 * @param keyByte
 * @return
 */
JNIEXPORT jbyteArray JNICALL
Java_com_fridaystu_thaicook_feature_main_MainActivity_AES_1ECB_1decrypt_1byte
        (JNIEnv *env, jobject instance, jbyteArray originByte, jbyteArray keyByte) {
    jsize len = (*env)->GetArrayLength(env, originByte); //获取长度
    unsigned char *originChar = as_unsigned_char_array(env, originByte);
    unsigned char *keyByteChar = as_unsigned_char_array(env, keyByte);
    uint8_t buffer[len];
    AES_ECB_decrypt(originChar, keyByteChar, buffer, len);
    int realLen = 0;
    for (int i = 0; i < len; i++) {
        if (buffer[i] == '\0') {
            break;
        }
        realLen++;
    }
    return as_byte_array(env, buffer, realLen);
}

/**
 * unsigned char 转化为 jbyteArray 返回
 * @param env
 * @param buf
 * @param len
 * @return
 */
jbyteArray as_byte_array(JNIEnv *env, unsigned char *buf, int len) {
    jbyteArray array = (*env)->NewByteArray(env, len);
    (*env)->SetByteArrayRegion(env, array, 0, len, (jbyte *)(buf));
    return array;
}

/**
 * jbyteArray 转化为 unsigned char 返回
 * @param env
 * @param array
 * @return
 */
unsigned char *as_unsigned_char_array(JNIEnv *env, jbyteArray array) {
    int len = (*env)->GetArrayLength(env, array);
    //unsigned char *buf = new unsigned char[len+1];
    unsigned char * buf = ( unsigned char * )malloc( (len+1) * sizeof( unsigned char ) );
    (*env)->GetByteArrayRegion(env, array, 0, len, (jbyte *)(buf));
    buf[len]='\0';
    return buf;
}


static void test_encrypt_ecb(void) {
#ifdef AES128
    uint8_t key[] = {0x2b, 0x7e, 0x15, 0x16, 0x28, 0xae, 0xd2, 0xa6, 0xab, 0xf7, 0x15, 0x88,
                     0x09,
                     0xcf, 0x4f, 0x3c};
    uint8_t out[] = {0x3a, 0xd7, 0x7b, 0xb4, 0x0d, 0x7a, 0x36, 0x60, 0xa8, 0x9e, 0xca, 0xf3,
                     0x24,
                     0x66, 0xef, 0x97};
#elif defined(AES192)
    uint8_t key[] = { 0x8e, 0x73, 0xb0, 0xf7, 0xda, 0x0e, 0x64, 0x52, 0xc8, 0x10, 0xf3, 0x2b, 0x80, 0x90, 0x79, 0xe5,
                    0x62, 0xf8, 0xea, 0xd2, 0x52, 0x2c, 0x6b, 0x7b};
  uint8_t out[] = { 0xbd, 0x33, 0x4f, 0x1d, 0x6e, 0x45, 0xf2, 0x5f, 0xf7, 0x12, 0xa2, 0x14, 0x57, 0x1f, 0xa5, 0xcc };
#elif defined(AES256)
    uint8_t key[] = { 0x60, 0x3d, 0xeb, 0x10, 0x15, 0xca, 0x71, 0xbe, 0x2b, 0x73, 0xae, 0xf0, 0x85, 0x7d, 0x77, 0x81,
                      0x1f, 0x35, 0x2c, 0x07, 0x3b, 0x61, 0x08, 0xd7, 0x2d, 0x98, 0x10, 0xa3, 0x09, 0x14, 0xdf, 0xf4 };
    uint8_t out[] = { 0xf3, 0xee, 0xd1, 0xbd, 0xb5, 0xd2, 0xa0, 0x3c, 0x06, 0x4b, 0x5a, 0x7e, 0x3d, 0xb1, 0x81, 0xf8 };
#endif

    uint8_t in[] = {0x6b, 0xc1, 0xbe, 0xe2, 0x2e, 0x40, 0x9f, 0x96, 0xe9, 0x3d, 0x7e, 0x11,
                    0x73,
                    0x93, 0x17, 0x2a};
    uint8_t buffer[16];

    AES_ECB_encrypt(in, key, buffer, 16);

    printf("ECB aesEncrypt:");

    if (0 == memcmp((char *) out, (char *) buffer, 16)) {
        printf("SUCCESS!\n");
        LOGI("SUCCESS!");
    } else {
        printf("FAILURE!\n");
        LOGE("FAILURE!\n");
    }
}

