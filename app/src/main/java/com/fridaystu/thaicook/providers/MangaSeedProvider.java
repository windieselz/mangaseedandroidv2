package com.fridaystu.thaicook.providers;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.fridaystu.thaicook.R;
import com.fridaystu.thaicook.helpers.UserHelper;
import com.fridaystu.thaicook.items.MangaChapter;
import com.fridaystu.thaicook.feature.manga.domain.MangaInfo;
import com.fridaystu.thaicook.items.MangaPage;
import com.fridaystu.thaicook.items.MangaSummary;
import com.fridaystu.thaicook.lists.MangaList;
import com.fridaystu.thaicook.services.HttpService;
import com.fridaystu.thaicook.services.dao.MangaPaymentDao;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by nv95 on 04.02.16.
 */
public class MangaSeedProvider extends MangaProvider {
	private Context mContext;
	private static final String baseUrl = "https://api.manga-seed.com/mangaseed/androidapiv14.0/";
	private static final int sorts[] = {R.string.sort_alphabetical, R.string.sort_popular, R.string.sort_rating, R.string.sort_updated};
	private static final String sortUrls[] = {"?az", "", "?rating", "?latest"};
	private static final int genres[] = {
			R.string.genre_all,  R.string.genre_extra, R.string.genre_action, R.string.genre_adventure, R.string.genre_comedy,
			R.string.genre_drama, R.string.genre_fantasy, R.string.genre_genderbender, R.string.genre_harem,
			R.string.genre_historical, R.string.genre_horror, R.string.genre_josei,
			R.string.genre_mecha, R.string.genre_mystery, R.string.genre_oneshot,
			R.string.genre_psychological, R.string.genre_romance, R.string.genre_school, R.string.genre_sci_fi,
			R.string.genre_seinen, R.string.genre_shounen, R.string.genre_slice_of_life, R.string.genre_sports,
			R.string.genre_supernatural, R.string.genre_tragedy, R.string.genre_not
	};
	private static final String genreUrls[] = {
			"", "999","1", "9", "14",
			"2",  "11", "19", "17",
			"3", "6", "22",
			"24", "7", "13",
			"21", "12", "4", "8",
			"18","15", "20", "10",
			"5", "23", "16"
	};

	private UserHelper user;

	public MangaSeedProvider(Context context) {
		super(context);
		mContext = context;
		user = UserHelper.getInstance();
	}

	private MangaInfo getMangaInfo(JSONObject item)
	{
		MangaInfo manga = new MangaInfo();
		try {
			String id = item.getString("id");

			manga.name = item.getString("name");
			manga.subtitle = item.getString("release");
			manga.genres = item.getString("genres");
			manga.path = baseUrl + "getCartoonPart/" + id;
			manga.preview = item.getString("img");
			manga.rating = (byte)  (20 * (int) Float.parseFloat(item.getString("rating"))) ;  //(Byte.parseByte("3.558") * 20);
			manga.provider = MangaSeedProvider.class;
			if (item.getString("status").equals("1")) {
				manga.status = MangaInfo.STATUS_COMPLETED;
			} else {
				manga.status = MangaInfo.STATUS_ONGOING;
			}

			if (item.getString("new").equals("1")) {
				manga.isNew = true;
			}

			if (item.getString("hot").equals("1")) {
				manga.isHot = true;
			}

			if (item.getString("vip").equals("1")) {
				manga.isVip = true;
			}

			manga.id = Integer.parseInt(id);
		} catch (final JSONException e) {
			Log.e("error", "Json parsing error: " + e.getMessage());
		}

		return manga;
	}

	@Override
	public MangaList getList(int page, int sort, int genre) throws Exception {

		if(user.isTimeToCheckDevice()) {
			Call<MangaPaymentDao> call = HttpService.getInstance().getService().checkDeviceAvailables(user.getMacAddr(),
					user.getEmail(), user.getUDID());
			call.enqueue(new Callback<MangaPaymentDao>() {
				@Override
				public void onResponse(@NonNull Call<MangaPaymentDao> call, @NonNull Response<MangaPaymentDao> response) {
					//Log.d("Response", response.body().toString());
					if (response.body()!=null) {
						if (response.body().getPurchase().trim().equals("purchased")) {
							user.setPurchase();
						} else {
							user.setUnPurchase();
						}

						user.setLastestCheckDevice();
					}

				}

				@Override
				public void onFailure(Call<MangaPaymentDao> call, Throwable t) {

				}
			});
		}


		MangaList list = new MangaList();
		String document;
		Uri.Builder builtUri;
		if(genre > 0){
			builtUri = Uri.parse(baseUrl + "getmangabycategory/"+ String.valueOf(genre)).buildUpon();
			builtUri.appendQueryParameter("page", String.valueOf(page));
			document = getRaw(builtUri.build().toString());

			if(page > 0)
			{
				return list;
			}

		}else {
			builtUri = Uri.parse(baseUrl + "getCartoon").buildUpon();
			builtUri.appendQueryParameter("page", String.valueOf(page));
			document = getRaw(builtUri.build().toString());
			//+ (genre == 0 ? "" : genreUrls[genre - 1] + "/")
			//+ (page + 1) + ".htm" + sortUrls[sort]
		}

		MangaInfo manga;


		//Log.d("info","mangaseed json :" + document);
		if (document != null) {
			try {
				JSONObject jsonObj = new JSONObject(document);
				JSONArray mangas = jsonObj.getJSONArray("result");
//				String updateMsg = jsonObj.getString("message_update");
//				if(!updateMsg.isEmpty())
//				{
//					new AlertDialog.Builder(mContext).setTitle(R.string.bug_report).setMessage(R.string.bug_report_message).create().show();
//					return list;
//				}

				for (int i = 0; i < mangas.length(); i++) {
					JSONObject item = mangas.getJSONObject(i);
					manga = getMangaInfo(item);
					list.add(manga);
				}

			} catch (final JSONException e) {
				Log.e("error", "Json parsing error: " + e.getMessage());
			}
		}

		return list;
	}

	@Override
	public MangaSummary getDetailedInfo(MangaInfo mangaInfo) {
		Log.d("info", "getDetailedInfo start: " + mangaInfo.name);
		if(user.getViewEvent()) {
			FirebaseFirestore db = FirebaseFirestore.getInstance();
			Map<String, Object> user = new HashMap<>();
			user.put("id", mangaInfo.id);
			db.collection("users_view")
					.add(user)
					.addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
						@Override
						public void onSuccess(DocumentReference documentReference) {
							Log.d("Firebase Cloud storage", "DocumentSnapshot added with ID: " + documentReference.getId());
						}
					})
					.addOnFailureListener(new OnFailureListener() {
						@Override
						public void onFailure(@NonNull Exception e) {
							Log.w("Firebase Cloud storage", "Error adding document", e);
						}
					});

			FirebaseAnalytics  mFirebaseAnalytics = FirebaseAnalytics.getInstance(mContext);
			Bundle params = new Bundle();
			params.putString("id", String.valueOf(mangaInfo.id));
			params.putString("device_id", this.user.getUid());
			mFirebaseAnalytics.logEvent("view_manga", params);
		}

		try {
			MangaSummary summary = new MangaSummary(mangaInfo);
			String document = getRaw( mangaInfo.path);
			Log.d("info","mangaseed part json :" + baseUrl +"getCartoonPart/"+ mangaInfo.path);
			MangaChapter chapter;
			//if (document != null) {
				try {
					JSONObject jsonObj = new JSONObject(document);
					JSONObject desc = jsonObj.getJSONObject("desc");
					summary.description = desc.getString("detail");

					//StringBuilder sb = new StringBuilder();
//					for (Element gnr : e.select("div.manga-genres a")) {
//						sb.append(",").append(gnr.text().trim());
//					}

					String sb = desc.getString("genres");
					List<String> genresList = Arrays.asList(sb.split(","));

					summary.genres = sb.length() > 2 ? TextUtils.join(",", genresList) : null;
					summary.preview = desc.getString("img");

					JSONArray chapters = jsonObj.getJSONArray("result");
					for (int i = 0; i < chapters.length(); i++) {
						JSONObject item = chapters.getJSONObject(i);
						chapter = new MangaChapter();
						chapter.name = item.getString("name");
						chapter.readLink = baseUrl + "getCartoonDetail/"+item.getString("part")+"/"+ mangaInfo.id;
						chapter.provider = summary.provider;
						chapter.isNew = (item.getString("isnew") == "1");
						summary.chapters.add(0, chapter);
					}

					summary.chapters.enumerate();
				} catch (final JSONException e) {
					Log.e("error", "Json parsing error: " + e.getMessage());
				}
			//}

			return summary;
		} catch (Exception e) {
			Log.e("error", "getDetailedInfo error: " + e.getMessage());
			return null;
		}
	}

	@Override
	public ArrayList<MangaPage> getPages(String readLink) {
		if(user.getReadEvent()) {
			FirebaseFirestore db = FirebaseFirestore.getInstance();
			Map<String, Object> user = new HashMap<>();
			user.put("link", readLink);
			db.collection("users_read")
					.add(user)
					.addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
						@Override
						public void onSuccess(DocumentReference documentReference) {
							Log.d("Firebase Cloud storage", "DocumentSnapshot added with ID: " + documentReference.getId());
						}
					})
					.addOnFailureListener(new OnFailureListener() {
						@Override
						public void onFailure(@NonNull Exception e) {
							Log.w("Firebase Cloud storage", "Error adding document", e);
						}
					});
		}

		ArrayList<MangaPage> pages = new ArrayList<>();
		Log.d("info","mangaseed pages json :" +readLink);
		try {

			String document = getRaw(readLink);
			MangaPage page;
			JSONObject jsonObj = new JSONObject(document);
			JSONArray imgs = jsonObj.getJSONArray("result");
			for (int i = 0; i < imgs.length(); i++) {
				JSONObject item = imgs.getJSONObject(i);
				page = new MangaPage(item.getString("img"));
				page.provider = MangaSeedProvider.class;
				pages.add(page);
			}

			return pages;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public String getPageImage(MangaPage mangaPage) {
		try {
			return mangaPage.path;
		} catch (Exception e) {
			return null;
		}
	}

	@Nullable
	@Override
	public MangaList search(String query, int page) throws Exception {

		Log.i("info", "search manga : "+ query);


		MangaList list = new MangaList();

		Uri.Builder builtUri = Uri.parse(baseUrl + "search").buildUpon();
		builtUri.appendQueryParameter("page", String.valueOf(page));
		builtUri.appendQueryParameter("query", query);
		MangaInfo manga;
		String document = getRaw(builtUri.build().toString());
		if (document != null) {
			try {
				JSONObject jsonObj = new JSONObject(document);
				JSONArray mangas = jsonObj.getJSONArray("result");
				for (int i = 0; i < mangas.length(); i++) {
					JSONObject item = mangas.getJSONObject(i);
					manga = getMangaInfo(item);
					list.add(manga);
				}

			} catch (final JSONException e) {
				Log.e("error", "Json parsing error: " + e.getMessage());
			}
		}

		return list;
	}

	@Override
	public String getName() {
		return "MangaSeed";
	}

	@Override
	public boolean hasGenres() {
		return true;
	}

	@Override
	public boolean hasSort() {
		return false;
	}

	@Override
	public boolean isSearchAvailable() {
		return true;
	}

	@Override
	public String[] getSortTitles(Context context) {
		return super.getTitles(context, sorts);
	}

	@Nullable
	@Override
	public String[] getGenresTitles(Context context) {
		return super.getTitles(context, genres);
	}
}
