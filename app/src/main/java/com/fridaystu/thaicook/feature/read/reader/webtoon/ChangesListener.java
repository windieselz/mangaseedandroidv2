package com.fridaystu.thaicook.feature.read.reader.webtoon;

/**
 * Created by admin on 03.08.17.
 */

public interface ChangesListener {

    void notifyDataSetChanged();
}
