package com.fridaystu.thaicook.feature.about.view

import android.content.Context
import android.text.Html
import androidx.lifecycle.ViewModel
import com.fridaystu.thaicook.R
import com.fridaystu.thaicook.utils.AppHelper


class AboutViewModel(
        private val context: Context
) : ViewModel() {

    val text: CharSequence by lazy { Html.fromHtml(AppHelper.getRawString(context, R.raw.about)) }

}