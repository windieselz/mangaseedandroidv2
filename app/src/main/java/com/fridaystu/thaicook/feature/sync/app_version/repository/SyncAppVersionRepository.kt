package com.fridaystu.thaicook.feature.sync.app_version.repository

import com.fridaystu.thaicook.feature.sync.app_version.api.SyncAppVersionApi
import com.fridaystu.thaicook.feature.sync.app_version.model.SyncAppVersionConverter
import com.fridaystu.thaicook.feature.sync.app_version.model.SyncAppVersion


class SyncAppVersionRepository(
        private val appVersionApi: SyncAppVersionApi
) {

    fun getUpdates(): List<SyncAppVersion> {
        val json = appVersionApi.getVersions()
        return SyncAppVersionConverter().convert(json)
    }

}