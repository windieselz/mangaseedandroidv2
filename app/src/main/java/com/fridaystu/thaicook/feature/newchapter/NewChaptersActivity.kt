package com.fridaystu.thaicook.feature.newchapter

import android.os.Bundle
import com.fridaystu.thaicook.R
import com.fridaystu.thaicook.core.activities.BaseActivity
import com.fridaystu.thaicook.feature.newchapter.view.NewChaptersFragment

/**
 * Created by nv95 on 17.04.16.
 */
class NewChaptersActivity : BaseActivity() {


	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setContentView(R.layout.activity_container)
		loadPage<NewChaptersFragment>(savedInstanceState)
	}

}
