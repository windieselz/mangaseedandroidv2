package com.fridaystu.thaicook.feature.extrafeature;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import androidx.fragment.app.Fragment;

import com.fridaystu.thaicook.R;
import com.fridaystu.thaicook.helpers.UserHelper;

public class BuyExtraFragment extends Fragment {
    private UserHelper userHelp;
    WebView web;
    public BuyExtraFragment() {
        super();
    }

    public static BuyExtraFragment newInstance() {
        BuyExtraFragment fragment = new BuyExtraFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_purchase, container, false);
        initInstances(rootView);
        return rootView;
    }

    private void initInstances(View rootView) {
        // init instance with rootView.findViewById here
        //setRetainInstance(true);
        userHelp = UserHelper.getInstance();
        web = (WebView) rootView.findViewById(R.id.webView);
        web.getSettings().setJavaScriptEnabled(true);
        String weburl = "http://api.manga-seed.com/mangaseed/cartoon.php/tmpay/tmform?device_id="+userHelp.getMacAddr()+"&udid="+userHelp.getUDID();
        web.loadUrl(weburl);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    /*
     * Save Instance State Here
     */
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    /*
     * Restore Instance State Here
     */
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }
}
