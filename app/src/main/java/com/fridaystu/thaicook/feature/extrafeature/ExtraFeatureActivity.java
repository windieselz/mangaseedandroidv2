package com.fridaystu.thaicook.feature.extrafeature;

import android.os.Bundle;

import androidx.appcompat.widget.Toolbar;

import com.fridaystu.thaicook.R;
import com.fridaystu.thaicook.core.activities.BaseAppActivity;


/**
 * Created by nv95 on 18.10.15.
 */
public class ExtraFeatureActivity extends BaseAppActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_extrafeature);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        getSupportActionBar().setDisplayShowHomeEnabled(true);
        if(savedInstanceState == null) {
            getSupportFragmentManager().
                    beginTransaction().replace(R.id.dynamic_fragment_frame_layout,new ExtraFragment()).commit();
        }

    }


}
