package com.fridaystu.thaicook.feature.sync.app_version.api

import org.json.JSONObject
import com.fridaystu.thaicook.BuildConfig
import com.fridaystu.thaicook.core.network.NetworkUtils


interface SyncAppVersionApi {

    fun getVersions(): JSONObject

    class Impl : SyncAppVersionApi {
        override fun getVersions(): JSONObject {
            return NetworkUtils.getJsonObject(BuildConfig.SELFUPDATE_URL)
        }
    }
}