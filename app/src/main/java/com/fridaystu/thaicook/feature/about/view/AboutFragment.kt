package com.fridaystu.thaicook.feature.about.view

import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.fragment_about.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import com.fridaystu.thaicook.R
import com.fridaystu.thaicook.core.fragment.BaseFragment
import com.fridaystu.thaicook.core.lifecycle.loadKoinModulesLifecycle
import com.fridaystu.thaicook.core.util.initToolbar
import com.fridaystu.thaicook.feature.about.di.aboutModule
import com.fridaystu.thaicook.utils.InternalLinkMovement


class AboutFragment : BaseFragment() {

    private val aboutViewModel: AboutViewModel by viewModel()

    override fun getLayout(): Int = R.layout.fragment_about

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        loadKoinModulesLifecycle(aboutModule)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initToolbar(getString(R.string.about))

        // display content
        setAboutText()
    }

    private fun setAboutText() = with(textView) {
        text = aboutViewModel.text
        movementMethod = InternalLinkMovement(null)
    }

}