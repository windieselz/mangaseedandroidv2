package com.fridaystu.thaicook.feature.main.adapter;

import androidx.recyclerview.widget.RecyclerView;

import com.fridaystu.thaicook.items.MangaChapter;

public interface OnChapterClickListener {
    void onChapterClick(int pos, MangaChapter chapter, RecyclerView.ViewHolder viewHolder);
    boolean onChapterLongClick(int pos, MangaChapter chapter, RecyclerView.ViewHolder viewHolder);
}
