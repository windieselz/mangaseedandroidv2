package com.fridaystu.thaicook.feature.newchapter.di

import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import com.fridaystu.thaicook.feature.newchapter.view.NewChapterViewModel


val newChapterModule = module(override = true) {

	viewModel {
		NewChapterViewModel(
				favouritesProvider = get(),
				newChaptersProvider = get(),
				connectionSource = get(),
				mainContext = get()
		)
	}

}