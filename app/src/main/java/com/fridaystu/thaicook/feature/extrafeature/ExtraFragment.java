package com.fridaystu.thaicook.feature.extrafeature;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.fridaystu.thaicook.R;
import com.fridaystu.thaicook.helpers.UserHelper;
import com.fridaystu.thaicook.services.HttpService;
import com.fridaystu.thaicook.services.dao.MangaPaymentDao;
import com.kaopiz.kprogresshud.KProgressHUD;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ExtraFragment extends Fragment {
        private UserHelper userHelp;
        private Button buybt,restorebt;
        private TextView extraStatus;
        EditText emailText;
        AlertDialog alertDialog;
        String currentEmail = "";
        KProgressHUD mDialog;

        public ExtraFragment() {
            super();
        }

        public static ExtraFragment newInstance() {
            ExtraFragment fragment = new ExtraFragment();
            Bundle args = new Bundle();
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.extra_fragment, container, false);
            initInstances(rootView);
            return rootView;
        }

        private void initInstances(View rootView) {
            userHelp = UserHelper.getInstance();
            buybt = (Button) rootView.findViewById(R.id.buybt);
            restorebt = (Button) rootView.findViewById(R.id.restorebt);

            buybt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    BuyExtraFragment buyextra = BuyExtraFragment.newInstance();
                    getActivity().getSupportFragmentManager().beginTransaction()
                            .replace(R.id.dynamic_fragment_frame_layout,buyextra ,"fragment_buy_extra")
                            .addToBackStack(null)
                            .commit();
                }

            });

            restorebt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getEmail();
                }

            });
            extraStatus = (TextView) rootView.findViewById(R.id.exstatus);
            checkStatusExtra();
        }

        @Override
        public void onStart() {
            super.onStart();
        }

        @Override
        public void onStop() {
            super.onStop();
        }

        /*
         * Save Instance State Here
         */
        @Override
        public void onSaveInstanceState(Bundle outState) {
            super.onSaveInstanceState(outState);
        }

        /*
         * Restore Instance State Here
         */
        @Override
        public void onActivityCreated(Bundle savedInstanceState) {
            super.onActivityCreated(savedInstanceState);
        }

    private void checkStatusExtra()
    {
        Call<MangaPaymentDao> call = HttpService.getInstance().getService().checkDeviceAvailables(userHelp.getMacAddr(),userHelp.getEmail(), userHelp.getUDID());
        call.enqueue(new Callback<MangaPaymentDao>() {
            @Override
            public void onResponse(Call<MangaPaymentDao> call, Response<MangaPaymentDao> response) {
                Log.d("Response", response.body().toString());
                if(response.body().getPurchase().trim().equals("purchased"))
                {
                    userHelp.setPurchase();
                }
                else
                {
                    userHelp.setUnPurchase();
                }

                showExtraStatus();
            }

            @Override
            public void onFailure(Call<MangaPaymentDao> call, Throwable t) {

            }
        });
    }

    private void showExtraStatus()
    {
        if(userHelp.isPurchase())
        {
            extraStatus.setText("สถานะ : Restore สำเร็จ ");
            extraStatus.setBackgroundColor(Color.parseColor("#a4c639"));
        }
        else
        {
            extraStatus.setText("สถานะ : ยังไม่ได้ Restore ");
            extraStatus.setBackgroundColor(Color.parseColor("#c65b39"));
        }
    }

    private void getEmail(){
        alertDialog = new android.app.AlertDialog.Builder(this.getActivity()).create();
        alertDialog.setTitle("Email Required");
        alertDialog.setMessage("กรุณาใส่ Email ที่เคยทำการซื้อไว้ด้วยนะครับ (ห้าม copy paste นะครับ)");
        alertDialog.setButton(android.app.AlertDialog.BUTTON_POSITIVE, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        checkPayment();
                    }
                });

        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE,"Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // Write your code here to execute after dialog
                        dialog.dismiss();
                    }
                });

        alertDialog.setOnShowListener( new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface arg0) {
                alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(getResources().getColor(android.R.color.white));
                alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(android.R.color.white));

            }
        });

//        Button positiveButton = alertDialog.getButton(0);
//        positiveButton.setTextColor(getResources().getColor(android.R.color.white));
//        Button negativeButton = alertDialog.getButton(1);
//        negativeButton.setTextColor(getResources().getColor(android.R.color.white));


//        new androidx.appcompat.app.AlertDialog.Builder(this.getActivity())
//                .setNegativeButton(android.R.string.no, null)
//                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        checkPayment();
//                    }
//                })
//                .setTitle("Email Required")
//                .setMessage("กรุณาใส่ Email ที่เคยทำการซื้อไว้ด้วยนะครับ (ห้าม copy paste นะครับ)")
//                .create().show();

        emailText = new EditText(this.getActivity());

        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        emailText.setLayoutParams(lp);
        alertDialog.setView(emailText);
        alertDialog.show();
    }

    public static boolean isValidEmail(String target) {
        return (!TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }

    private void checkPayment()
    {
        String email = emailText.getText().toString();
        currentEmail = email;
        alertDialog.dismiss();
        if(!isValidEmail(email)){
            Toast.makeText(this.getActivity(), "กรุณาใส่ email ให้ถูกต้อง (ห้าม copy paste)",
                    Toast.LENGTH_SHORT)
                    .show();

            return ;
        }

        //UserHelper.getInstance().getMacAddr()
        mDialog = KProgressHUD.create(this.getActivity())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setDetailsLabel(getString(R.string.loading))
                .setCancellable(true)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();

        Log.d("HOME", "REFRESH DATA" + userHelp.getMacAddr() + "/"+ email + "/"+ userHelp.getUDID());

        Call<MangaPaymentDao> call = HttpService.getInstance().getService().checkPayment(userHelp.getMacAddr(),email,userHelp.getUDID());
        call.enqueue(new Callback<MangaPaymentDao>() {
            @Override
            public void onResponse(Call<MangaPaymentDao> call, Response<MangaPaymentDao> response) {
                String alert = "ไม่สามารถทำการ restore ได้";
                if(response.body().getPurchase().trim().equals("purchased"))
                {
                    alert = "Restore success";
                    userHelp.setPurchase();
                    userHelp.setEmail(currentEmail);
                }

                mDialog.dismiss();

                showAlertExtraStatus(alert);

            }

            @Override
            public void onFailure(Call<MangaPaymentDao> call, Throwable t) {
                //swipeRefresh.setRefreshing(false);

                Log.e("http error", t.getMessage());
                mDialog.dismiss();
                showAlertExtraStatus(t.getMessage());
            }
        });
    }

    private void showAlertExtraStatus(String status)
    {
        Toast.makeText(this.getActivity(), status,
                Toast.LENGTH_SHORT)
                .show();

        showExtraStatus();
    }
}


