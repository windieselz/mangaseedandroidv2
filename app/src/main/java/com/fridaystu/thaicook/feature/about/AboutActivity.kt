package com.fridaystu.thaicook.feature.about

import android.os.Bundle
import com.fridaystu.thaicook.R
import com.fridaystu.thaicook.core.activities.BaseActivity
import com.fridaystu.thaicook.feature.about.view.AboutFragment

/**
 * Created by nv95 on 12.01.16.
 */
class AboutActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_container)
        loadPage<AboutFragment>(savedInstanceState)
    }

}
