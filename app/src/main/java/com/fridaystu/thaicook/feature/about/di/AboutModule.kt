package com.fridaystu.thaicook.feature.about.di

import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import com.fridaystu.thaicook.feature.about.view.AboutViewModel


val aboutModule = module {

    viewModel { AboutViewModel(get()) }

}