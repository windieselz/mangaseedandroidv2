package com.fridaystu.thaicook.feature.sync.app_version.di

import org.koin.dsl.module
import com.fridaystu.thaicook.feature.sync.app_version.api.SyncAppVersionApi
import com.fridaystu.thaicook.feature.sync.app_version.repository.SyncAppVersionRepository


val updateAppVersionModule = module {

    single<SyncAppVersionApi> { SyncAppVersionApi.Impl() }

    single { SyncAppVersionRepository(get()) }

}