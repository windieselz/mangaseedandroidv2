package com.fridaystu.thaicook.core.activities;

import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;

import androidx.annotation.ColorRes;
import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;

import com.facebook.ads.AudienceNetworkAds;
import com.google.android.gms.ads.LoadAdError;
import com.fridaystu.thaicook.helpers.UserHelper;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.interstitial.InterstitialAd;
import com.google.android.gms.ads.interstitial.InterstitialAdLoadCallback;

import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.snackbar.Snackbar;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetView;

import com.fridaystu.thaicook.R;
import com.fridaystu.thaicook.utils.LayoutUtils;
import com.fridaystu.thaicook.core.network.NetworkUtils;
/*
import com.tapdaq.sdk.Tapdaq;
import com.tapdaq.sdk.common.TMAdError;
import com.tapdaq.sdk.common.TMAdType;
import com.tapdaq.sdk.listeners.TMAdListener;
import com.tapdaq.sdk.listeners.TMInitListener;
import com.tapdaq.sdk.model.rewards.TDReward;
*/

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Random;

/**
 * Created by nv95 on 19.02.16.
 */
public abstract class BaseAppActivity extends AppCompatActivity {

    private static final int REQUEST_PERMISSION = 112;
    protected UserHelper mUserHelp;
    private boolean mActionBarVisible = false;
    private boolean mHomeAsUpEnabled = false;
//    protected boolean showTapDaqAlready = false;
    private int mTheme = 0;
    protected InterstitialAd mInterstitialAd;
    protected com.facebook.ads.InterstitialAd fInterstitialAd;
    private final String TAG = BaseAppActivity.class.getSimpleName();
    protected String currentAdsGroup;

    @Nullable
    private ArrayList<WeakReference<AsyncTask>> mLoaders;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mTheme = LayoutUtils.getAppTheme(this);
        setTheme(LayoutUtils.getAppThemeRes(mTheme));
        mUserHelp = UserHelper.getInstance();

//        TapdaqConfig config = Tapdaq.getInstance().config();
//        config.setAutoReloadAds(true);
//        config.setUserSubjectToGdprStatus(STATUS.TRUE); //GDPR declare if user is in EU
//        config.setConsentStatus(STATUS.TRUE); //GDPR consent must be obtained from the user
//        config.setAgeRestrictedUserStatus(STATUS.FALSE); //Is user subject to COPPA or GDPR age restrictions
//
//
//        Tapdaq.getInstance().initialize(this, "61359f2e08fe6c2d735d6bd4", "3ccb9556-c98c-472d-a1b9-fe0fa08b18d8", config, new TapdaqInitListener());


        AudienceNetworkAds.initialize(this);
        MobileAds.initialize(this, new OnInitializationCompleteListener() {
            @Override
            public void onInitializationComplete(InitializationStatus initializationStatus) {
            }
        });
    }

    public boolean isDarkTheme() {
        return mTheme > 7;
    }

    public void enableHomeAsUp() {
        final ActionBar actionBar = getSupportActionBar();
        if (actionBar != null && !mHomeAsUpEnabled) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            mHomeAsUpEnabled = true;
        }
    }

    public void enableHomeAsClose() {
        final ActionBar actionBar = getSupportActionBar();
        if (actionBar != null && !mHomeAsUpEnabled) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeAsUpIndicator(R.drawable.ic_cancel_light);
            mHomeAsUpEnabled = true;
        }
    }

    public void disableTitle() {
        final ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayShowTitleEnabled(false);
        }
    }

    @Override
    public void setSupportActionBar(@Nullable Toolbar toolbar) {
        super.setSupportActionBar(toolbar);
        mActionBarVisible = toolbar != null;
    }

    @Deprecated
    protected void setupToolbarScrolling(Toolbar toolbar) {
        setToolbarScrollingLock(toolbar, false);
    }

    @Deprecated
    protected void setToolbarScrollingLock(Toolbar toolbar, boolean lock) {
        if (toolbar == null || !(toolbar.getParent() instanceof AppBarLayout)) {
            return;
        }
        boolean scrolls = !lock && PreferenceManager.getDefaultSharedPreferences(this).getBoolean("hide_toolbars", true);
        AppBarLayout.LayoutParams params = (AppBarLayout.LayoutParams) toolbar.getLayoutParams();
        params.setScrollFlags(scrolls ? AppBarLayout.LayoutParams.SCROLL_FLAG_SCROLL | AppBarLayout.LayoutParams.SCROLL_FLAG_ENTER_ALWAYS : 0);
    }

    public void setSupportActionBar(@IdRes int toolbarId) {
        setSupportActionBar((Toolbar) findViewById(toolbarId));
    }

    public void hideActionBar() {
        final ActionBar actionBar = getSupportActionBar();
        if (actionBar != null && mActionBarVisible) {
            mActionBarVisible = false;
            actionBar.hide();
        }
    }

    public void showActionBar() {
        final ActionBar actionBar = getSupportActionBar();
        if (actionBar != null && !mActionBarVisible) {
            mActionBarVisible = true;
            actionBar.show();
        }
    }

    public void toggleActionBar() {
        final ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            if (!mActionBarVisible) {
                mActionBarVisible = true;
                actionBar.show();
            } else {
                mActionBarVisible = false;
                actionBar.hide();
            }
        }
    }

    public boolean isActionBarVisible() {
        return mActionBarVisible;
    }

    public int getActivityTheme() {
        return mTheme;
    }

    public void setSubtitle(@Nullable CharSequence subtitle) {
        final ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setSubtitle(subtitle);
        }
    }

    public void setSubtitle(@StringRes int subtitle) {
        setSubtitle(getString(subtitle));
    }

    public void enableTransparentStatusBar(@ColorRes int color) {
        if (Build.VERSION.SDK_INT >= 21) {
            Window window = getWindow();
            window.getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
            if (color != 0) {
                window.setStatusBarColor(ContextCompat.getColor(this, color));
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home && mHomeAsUpEnabled) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    public void keepScreenOn() {
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }

    public boolean checkPermission(String permission) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        }
        if (ContextCompat.checkSelfPermission(this,
                permission) == PackageManager.PERMISSION_GRANTED) {
            return true;
        }
        if (!ActivityCompat.shouldShowRequestPermissionRationale(this, permission)) {
            ActivityCompat.requestPermissions(this,
                    new String[]{permission},
                    REQUEST_PERMISSION);
        }
        return false;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        if (requestCode == REQUEST_PERMISSION) {
            for (int i = 0; i < permissions.length; i++) {
                if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                    onPermissionGranted(permissions[i]);
                }
            }
        }
    }

    protected void onPermissionGranted(String permission) {

    }

    public void showToast(CharSequence text, int gravity, int delay) {
        final Toast toast = Toast.makeText(this, text, delay);
        toast.setGravity(gravity, 0, 0);
        toast.show();
    }

    public void showToast(@StringRes int text, int gravity, int delay) {
        showToast(getString(text), gravity, delay);
    }

    public boolean checkConnectionWithSnackbar(View view) {
        if (NetworkUtils.checkConnection(this)) {
            return true;
        } else {
            Snackbar.make(view, R.string.no_network_connection, Snackbar.LENGTH_SHORT).show();
            return false;
        }
    }

    public void registerLoaderTask(AsyncTask task) {
        if (mLoaders == null) {
            mLoaders = new ArrayList<>();
        }
        mLoaders.add(new WeakReference<>(task));
    }

    @Override
    protected void onDestroy() {
        if (mLoaders != null) {
            for (WeakReference<AsyncTask> o : mLoaders) {
                AsyncTask task = o.get();
                if (task != null && task.getStatus() != AsyncTask.Status.FINISHED) {
                    task.cancel(true);
                }
            }
        }
        mLoaders = null;
        super.onDestroy();
    }

    protected boolean showcase(final View view, @StringRes int title, @StringRes int body) {
        return showcase(view, title, body, false);
    }

    protected boolean showcase(final View view, @StringRes int title, @StringRes int body, boolean tint) {
        boolean dark = isDarkTheme();
        if (view != null && view.getVisibility() == View.VISIBLE
                && !getSharedPreferences("tips", MODE_PRIVATE).getBoolean(getClass().getSimpleName() + "_" + view.getId(), false)) {
            TapTargetView.showFor(this,
                    TapTarget.forView(view, getString(title), getString(body))
                            .transparentTarget(!tint)
                            .textColorInt(Color.WHITE)
                            .dimColorInt(LayoutUtils.getAttrColor(this, R.attr.colorPrimaryDark))
                            .tintTarget(tint),
                    new TapTargetView.Listener() {          // The listener can listen for regular clicks, long clicks or cancels
                        @Override
                        public void onTargetClick(TapTargetView view1) {
                            super.onTargetClick(view1);
                        }
                    });
            SharedPreferences prefs = getSharedPreferences("tips", MODE_PRIVATE);
            prefs.edit().putBoolean(BaseAppActivity.this.getClass().getSimpleName() + "_" + view.getId(), true).apply();
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param menuItemId
     * @param title
     * @param body
     * @return true if showcase shown
     */
    protected boolean showcase(@IdRes final int menuItemId, @StringRes int title, @StringRes int body) {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        return toolbar != null && showcase(toolbar.findViewById(menuItemId), title, body, false);
    }

    /**
     * @return true only once for activity
     */
    protected boolean isFirstStart() {
        SharedPreferences prefs = getSharedPreferences("tips", MODE_PRIVATE);
        if (prefs.getBoolean(getClass().getName(), true)) {
            prefs.edit().putBoolean(getClass().getName(), false).apply();
            return true;
        }
        return false;
    }

    private int getRandomNumber() {
        Random rn = new Random();
        return rn.nextInt(5) + 0;
    }


    protected boolean shouldLoad(String adsGroup) {
        Long ts = System.currentTimeMillis() / 1000;
        Long lastestAds = mUserHelp.getAdsLoadedTimestamp("global");
        Log.d("admob", "current time : " + ts + " , last showing : " + lastestAds + ", Gap : " + mUserHelp.getAdsGap("global"));
        if (ts - lastestAds < mUserHelp.getAdsGap("global")) {
            return false;
        }

        Long lastestGroupAds = mUserHelp.getAdsLoadedTimestamp(adsGroup);
        if (ts - lastestGroupAds < mUserHelp.getAdsGap(adsGroup)) {
            return false;
        }

        return true;
    }

    public String getAdsID(String adsGroup) {
        boolean debugMode = false;
        String Adtype = mUserHelp.getAdType();
        if (Adtype.equals("tapdaq")) {
            return "tapdaq_id";
        }

        if (debugMode && Adtype.equals("admob")) {
            return "ca-app-pub-3940256099942544/1033173712";
        }

        if (adsGroup.equals("home") && Adtype.equals("admob")) {
            return "ca-app-pub-6228812479912868/1421423228";
        }

        if (adsGroup.equals("info") && Adtype.equals("admob")) {
            return "ca-app-pub-6228812479912868/7711978334";
        }

        if (adsGroup.equals("gallery") && Adtype.equals("admob")) {
            return "ca-app-pub-6228812479912868/6210715473";
        }

        if (adsGroup.equals("home") && Adtype.equals("facebook")) {
            return "200797447399757_530313534448145";
        }

        if (adsGroup.equals("info") && Adtype.equals("facebook")) {
            return "200797447399757_534125150733650";
        }

        if (adsGroup.equals("gallery") && Adtype.equals("facebook")) {
            return "200797447399757_534125280733637";
        }

        return "";
    }

    public void updateAdsView() {

//        Log.i("MEDIATION", "show ads  , video : "+ Tapdaq.getInstance().isVideoReady(this, getPlacementTag()));
//        Log.i("MEDIATION", "show ads  , reward : "+ Tapdaq.getInstance().isRewardedVideoReady(this, getPlacementTag()));
//        Log.i("MEDIATION", "show ads  , interestial : "+ Tapdaq.getInstance().isInterstitialReady(this, getPlacementTag()));
       /* Log.i("MEDIATION", "decide show ads ");
        if (!showTapDaqAlready && shouldLoad("global")) {
            Log.i("MEDIATION", "ready show ads ");

            if (Tapdaq.getInstance().isVideoReady(this, getPlacementTag())) {
                Tapdaq.getInstance().showVideo(this, getPlacementTag(), new TapdaqAdsListener(TMAdType.VIDEO_INTERSTITIAL));
                showTapDaqAlready = true;
            } else if (Tapdaq.getInstance().isRewardedVideoReady(this, getPlacementTag())) {
                Tapdaq.getInstance().showRewardedVideo(this, getPlacementTag(), new TapdaqAdsListener(TMAdType.REWARD_INTERSTITIAL));
                showTapDaqAlready = true;
            } else if (Tapdaq.getInstance().isInterstitialReady(this, getPlacementTag())) {
                Tapdaq.getInstance().showInterstitial(this, getPlacementTag(), new TapdaqAdsListener(TMAdType.STATIC_INTERSTITIAL));
                showTapDaqAlready = true;
            }

            if (showTapDaqAlready) {
                mUserHelp.setAdsLoadedTimestamp(currentAdsGroup);
            }
        }*/
    }

    private String getPlacementTag() {
        return "defaultads";
    }

    public void loadAds(String adsGroup) {
        currentAdsGroup = adsGroup;
        Log.d("admob", "Starting to show ads type : " + adsGroup);
        Log.i("MEDIATION", "load ads ");
        //Tapdaq.getInstance().loadInterstitial(this, "default", new TapdaqAdsListener(TMAdType.VIDEO_INTERSTITIAL));

        //Log.d("admob", "should load ads : "+ shouldLoad(adsGroup) );
        if (!mUserHelp.isPurchase() && shouldLoad(adsGroup) && !getAdsID(adsGroup).isEmpty()) {
            String adsId = getAdsID(adsGroup);

            Log.d("admob", "Starting to show ads ID : " + adsId + " Ads Type :" + mUserHelp.getAdType());
            Handler handler = new Handler();
            String Adtype = mUserHelp.getAdType();
            Log.d("admob", "show ad type :" + Adtype);
            if (Adtype.equals("admob")) {
                Log.d("admob", "load admob ads");
//                mInterstitialAd = new InterstitialAd(this);
//                mInterstitialAd.setAdUnitId(adsId);
//                mInterstitialAd.loadAd(new AdRequest.Builder().build());
//                mInterstitialAd.setAdListener(new AdListener() {
//                    @Override
//                    public void onAdLoaded() {
//                        // Code to be executed when an ad finishes loading.
//                        if (mInterstitialAd.isLoaded()) {
//                            //mInterstitialAd.show();
//
//                            handler.postDelayed(new Runnable() {
//                                public void run() {
//
//                                    mInterstitialAd.show();
//                                }
//                            }, 1000 * getRandomNumber()); // Show the ad after 15 minutes
//                        }
//                    }
//
//                    @Override
//                    public void onAdFailedToLoad(int errorCode) {
//                        // Code to be executed when an ad request fails.
//                    }
//
//                    @Override
//                    public void onAdOpened() {
//                        // Code to be executed when the ad is displayed.
//                    }
//
//                    @Override
//                    public void onAdClicked() {
//                        // Code to be executed when the user clicks on an ad.
//                    }
//
//                    @Override
//                    public void onAdLeftApplication() {
//                        // Code to be executed when the user has left the app.
//                    }
//
//                    @Override
//                    public void onAdClosed() {
//                        // Code to be executed when the interstitial ad is closed.
//                    }
//                });

                AdRequest adRequest = new AdRequest.Builder().build();

                InterstitialAd.load(this, adsId, adRequest,
                        new InterstitialAdLoadCallback() {
                            @Override
                            public void onAdLoaded(@NonNull InterstitialAd interstitialAd) {
                                // The mInterstitialAd reference will be null until
                                // an ad is loaded.
                                mInterstitialAd = interstitialAd;
                                Log.i(TAG, "admob onAdLoaded");
                                mUserHelp.setAdsLoadedTimestamp(adsGroup);

                                handler.postDelayed(new Runnable() {
                                    public void run() {

                                        if (mInterstitialAd != null) {
                                            mInterstitialAd.show(BaseAppActivity.this);
                                        } else {
                                            Log.d("TAG", "The interstitial ad wasn't ready yet.");
                                        }
                                    }
                                }, 1000 * getRandomNumber()); // Show the ad after 15 minutes

                            }

                            @Override
                            public void onAdFailedToLoad(@NonNull LoadAdError loadAdError) {
                                // Handle the error
                                Log.i("admob", loadAdError.getMessage());
                                // Toast.makeText(getApplicationContext(), loadAdError.getMessage(), Toast.LENGTH_LONG).show();
                                mInterstitialAd = null;
                            }
                        });
            } else if (Adtype.equals("tapdaq")) {

                Log.i("MEDIATION-SAMPLE", "load ads ");
                /*Tapdaq.getInstance().loadVideo(this, getPlacementTag(), new TapdaqAdsListener(TMAdType.VIDEO_INTERSTITIAL));
                //Tapdaq.getInstance().loadRewardedVideo(this,  getPlacementTag(), new TapdaqAdsListener(TMAdType.REWARD_INTERSTITIAL));
                Tapdaq.getInstance().loadInterstitial(this, getPlacementTag(), new TapdaqAdsListener(TMAdType.STATIC_INTERSTITIAL));
*/

            } else {

                Log.d(TAG, "load fb ads");
//                fInterstitialAd = new com.facebook.ads.InterstitialAd(this, adsId);
//                fInterstitialAd.setAdListener(new InterstitialAdListener() {
//                    @Override
//                    public void onInterstitialDisplayed(Ad ad) {
//                        // Interstitial ad displayed callback
//                        Log.e(TAG, "Interstitial ad displayed.");
//                    }
//
//                    @Override
//                    public void onInterstitialDismissed(Ad ad) {
//                        // Interstitial dismissed callback
//                        Log.e(TAG, "Interstitial ad dismissed.");
//                    }
//
//                    @Override
//                    public void onError(Ad ad, AdError adError) {
//                        // Ad error callback
//                        Log.e(TAG, "Interstitial ad failed to load: " + adError.getErrorMessage());
//                    }
//
//                    @Override
//                    public void onAdLoaded(Ad ad) {
//                        // Interstitial ad is loaded and ready to be displayed
//                        Log.d(TAG, "Interstitial ad is loaded and ready to be displayed!");
//                        // Show the ad
//
//                        handler.postDelayed(new Runnable() {
//                            public void run() {
//
//                                fInterstitialAd.show();
//                            }
//                        }, 1000 * getRandomNumber()); // Show the ad after 15 minutes
//                    }
//
//                    @Override
//                    public void onAdClicked(Ad ad) {
//                        // Ad clicked callback
//                        Log.d(TAG, "Interstitial ad clicked!");
//                    }
//
//                    @Override
//                    public void onLoggingImpression(Ad ad) {
//                        // Ad impression logged callback
//                        Log.d(TAG, "Interstitial ad impression logged!");
//                    }
//                });
//
//                // For auto play video ads, it's recommended to load the ad
//                // at least 30 seconds before it is shown
//                fInterstitialAd.loadAd();
            }
        }

    }


    //############################################ Tapdaq implementation ##########################

  /*  public class TapdaqInitListener extends TMInitListener {

        public void didInitialise() {
            super.didInitialise();
            // Ads may now be requested

            Log.i("MEDIATION", "didInitialise");
        }

        @Override
        public void didFailToInitialise(TMAdError error) {
            super.didFailToInitialise(error);
            //Tapdaq failed to initialise
            Log.i("MEDIATION", "did failed Initialise");
            String str = String.format(Locale.ENGLISH, "Adtype: %s didFailToLoad : %d - %s", mUserHelp.getAdType(), error.getErrorCode(), error.getErrorMessage());

            for (String key : error.getSubErrors().keySet()) {
                for (TMAdError value : error.getSubErrors().get(key)) {
                    String subError = String.format(Locale.ENGLISH, "%s - %d: %s", key, value.getErrorCode(), value.getErrorMessage());
                    str = str.concat("\n ");
                    str = str.concat(subError);
                }
            }

            Toast.makeText(getApplicationContext(), str, Toast.LENGTH_LONG).show();
        }
    }

    public class TapdaqAdsListener extends TMAdListener {
        private int mType;

        public TapdaqAdsListener(int type) {
            mType = type;
        }

//        void AdListener(int type) {
//            mType = type;
//        }

        @Override
        public void didLoad() {
            //updateView();
            updateAdsView();
            Log.i("MEDIATION-SAMPLE", "didLoad " + TMAdType.getString(mType));
            //mLogListAdapter.insert("didLoad " + TMAdType.getString(mType), 0);
        }

        @Override
        public void didFailToLoad(TMAdError error) {
            //updateView();

            String str = String.format(Locale.ENGLISH, " Adtype: %s didFailToLoad %s: %d - %s", mUserHelp.getAdType(), TMAdType.getString(mType), error.getErrorCode(), error.getErrorMessage());

            for (String key : error.getSubErrors().keySet()) {
                for (TMAdError value : error.getSubErrors().get(key)) {
                    String subError = String.format(Locale.ENGLISH, "%s - %d: %s", key, value.getErrorCode(), value.getErrorMessage());
                    str = str.concat("\n ");
                    str = str.concat(subError);
                }
            }

            Log.i("MEDIATION-SAMPLE", str);
            //mLogListAdapter.insert(str, 0);
            Toast.makeText(getApplicationContext(), str, Toast.LENGTH_LONG).show();


        }

        @Override
        public void didRefresh() {
            Log.i("MEDIATION-SAMPLE", "didRefresh " + TMAdType.getString(mType));
            //mLogListAdapter.insert("didRefresh " + TMAdType.getString(mType), 0);
        }

        @Override
        public void willDisplay() {
            Log.i("MEDIATION-SAMPLE", "willDisplay " + TMAdType.getString(mType));
            //mLogListAdapter.insert("willDisplay " + TMAdType.getString(mType), 0);
        }

        @Override
        public void didDisplay() {
            Log.i("MEDIATION-SAMPLE", "didDisplay " + TMAdType.getString(mType));
            //mLogListAdapter.insert("didDisplay " + TMAdType.getString(mType), 0);
        }

        @Override
        public void didClick() {
            Log.i("MEDIATION-SAMPLE", "didClick " + TMAdType.getString(mType));
            //mLogListAdapter.insert("didClick " + TMAdType.getString(mType), 0);
        }

        @Override
        public void didVerify(TDReward reward) {
            String str = String.format(Locale.ENGLISH, "didVerify %s: Reward name: %s. Value: %d. Valid: %b. Custom Json: %s", TMAdType.getString(mType), reward.getName(), reward.getValue(), reward.isValid(), reward.getCustom_json().toString());
            Log.i("MEDIATION-SAMPLE", str);
            //mLogListAdapter.insert(str, 0);
        }

        @Override
        public void didClose() {
            //updateView();
            Log.i("MEDIATION-SAMPLE", "didClose " + TMAdType.getString(mType));
            //mLogListAdapter.insert("didClose " + TMAdType.getString(mType), 0);
        }
    }*/
}
