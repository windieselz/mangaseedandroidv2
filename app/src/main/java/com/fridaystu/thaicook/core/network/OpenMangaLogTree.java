package com.fridaystu.thaicook.core.network;

import org.jetbrains.annotations.Nullable;
import com.fridaystu.thaicook.BuildConfig;

import timber.log.Timber;

public class OpenMangaLogTree extends Timber.DebugTree {

    @Override
    protected boolean isLoggable(@Nullable final String tag, final int priority) {
        return BuildConfig.DEBUG;
    }
}
