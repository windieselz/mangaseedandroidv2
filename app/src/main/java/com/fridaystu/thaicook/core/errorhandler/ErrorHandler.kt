package com.fridaystu.thaicook.core.errorhandler

import com.fridaystu.thaicook.core.exeption.ErrorExceptionMessage

/**
 * CoroutineExceptionHandler can't have multiple Handlers
 * this help for this
 */
interface ErrorHandler {
    /**
     * Handle exception when happened in app
     */
    fun handle(error: ErrorExceptionMessage): Boolean
}