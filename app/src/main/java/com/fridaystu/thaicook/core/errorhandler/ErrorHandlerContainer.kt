package com.fridaystu.thaicook.core.errorhandler

import com.fridaystu.thaicook.core.exeption.ErrorExceptionMessage
import com.fridaystu.thaicook.core.extention.loge

/**
 * Contains multiple error handlers for adding custom
 * behavior
 */
class ErrorHandlerContainer : ErrorHandler {
    companion object {
        private const val TAG = "ErrorHandlerContainer"
    }
    val errorHandlers = mutableListOf<ErrorHandler>()
    override fun handle(error: ErrorExceptionMessage): Boolean {
        // for debug
        error.error.loge(TAG)
        // return handled result
        return errorHandlers.any { it.handle(error) }
    }

}

inline fun ErrorHandlerContainer.addHandler(crossinline action: (ErrorExceptionMessage)->Boolean) {
    errorHandlers += object : ErrorHandler {
        override fun handle(error: ErrorExceptionMessage): Boolean {
            return action.invoke(error)
        }
    }
}

inline fun ErrorHandlerContainer.addHandlerBeforeAll(crossinline action: (ErrorExceptionMessage)->Boolean) {
    errorHandlers.add(0, object : ErrorHandler {
        override fun handle(error: ErrorExceptionMessage): Boolean {
            return action.invoke(error)
        }
    })
}