package com.fridaystu.thaicook.dialogs;

/**
 * Created by nv95 on 09.10.16.
 */

public interface NavigationListener {
    void onPageChange(int page);
}
