package com.fridaystu.thaicook.components;

/**
 * Created by admin on 24.07.17.
 */

public interface IntegerPreference {

    int getValue();
}
