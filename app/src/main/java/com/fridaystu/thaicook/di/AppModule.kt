package com.fridaystu.thaicook.di

import org.koin.dsl.module
import com.fridaystu.thaicook.core.sources.ConnectionSource
import com.fridaystu.thaicook.helpers.StorageHelper
import com.fridaystu.thaicook.providers.FavouritesProvider
import com.fridaystu.thaicook.providers.NewChaptersProvider
import com.fridaystu.thaicook.providers.staff.MangaProviderManager


val appModule = module {

	single { MangaProviderManager(get()) }

	single { ConnectionSource(get()) }

}

val dbModules = module {

	single { StorageHelper(get()) }

	single { FavouritesProvider(get(), get()) }

	single { NewChaptersProvider(get(), get()) }

}