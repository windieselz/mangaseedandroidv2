package com.fridaystu.thaicook.di

import androidx.lifecycle.LifecycleOwner
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import org.koin.core.qualifier.TypeQualifier
import org.koin.dsl.module
import com.fridaystu.thaicook.core.errorhandler.DefaultErrorHandler
import com.fridaystu.thaicook.core.errorhandler.ErrorHandlerContainer
import com.fridaystu.thaicook.core.exeption.ErrorExceptionMessage
import com.fridaystu.thaicook.core.extention.loge
import com.fridaystu.thaicook.core.fragment.BaseView
import com.fridaystu.thaicook.core.lifecycle.JobController


/**
 * Only for feature scope
 */
fun lifecycleFeatureModule(owner: LifecycleOwner) = module {

	scope(TypeQualifier(owner::class)) {

		scoped { JobController(owner) }

		scoped {
			ErrorHandlerContainer().apply {
				if (owner is BaseView) {
					errorHandlers += DefaultErrorHandler(owner)
				}
			}
		}

		scoped {

			val jobController = get<JobController>()
			val errorHandler = get<ErrorHandlerContainer>()

			jobController.rootJob + CoroutineExceptionHandler { _, throwable ->
				throwable.loge(owner.javaClass.name)
				if (owner is BaseView) {
					CoroutineScope(jobController.rootJob).launch {
						errorHandler.handle(ErrorExceptionMessage(throwable))
					}
				} else {
					throw throwable
				}
			}
		}

	}


}