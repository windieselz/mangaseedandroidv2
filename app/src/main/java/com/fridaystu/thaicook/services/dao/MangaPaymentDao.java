package com.fridaystu.thaicook.services.dao;

import com.google.gson.annotations.SerializedName;


/**
 * Created by kittipongkulapruk on 9/3/15 AD.
 */
public class MangaPaymentDao {

    @SerializedName("purchase")
    private String purchase;

    public String getPurchase() {
        return purchase;
    }

    public void setPurchase(String purchase) {
        this.purchase = purchase;
    }
}
