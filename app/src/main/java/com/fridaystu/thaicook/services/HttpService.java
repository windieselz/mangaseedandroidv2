package com.fridaystu.thaicook.services;


import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;



public class HttpService {

    private static HttpService instance;

    public static HttpService getInstance() {
        if (instance == null)
            instance = new HttpService();
        return instance;
    }

    private ApiService mService;
    private HttpService() {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://api.manga-seed.com/mangaseed/androidapiv14.0/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();


        mService = retrofit.create(ApiService.class);
    }

    public ApiService getService(){
        return mService;
    }
}



