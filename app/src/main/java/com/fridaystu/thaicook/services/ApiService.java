package com.fridaystu.thaicook.services;

import com.fridaystu.thaicook.services.dao.MangaPaymentDao;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;


public interface ApiService {

    @GET("checkPayment?nogzip=1")
    Call<MangaPaymentDao> checkPayment(@Query("deviceid") String deviceid, @Query("email") String email, @Query("udid") String udid);

    @GET("checkDeviceAvailable?nogzip=1")
    Call<MangaPaymentDao> checkDeviceAvailables(@Query("deviceid") String deviceid,@Query("email") String email, @Query("udid") String udid);

}