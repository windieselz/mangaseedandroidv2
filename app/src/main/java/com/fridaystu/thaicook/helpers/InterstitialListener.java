package com.fridaystu.thaicook.helpers;

import android.util.Log;

/*
import com.tapdaq.sdk.common.TMAdError;
import com.tapdaq.sdk.common.TMAdType;
import com.tapdaq.sdk.listeners.TMAdListener;
import com.tapdaq.sdk.model.rewards.TDReward;


import java.util.Locale;

public class InterstitialListener extends TMAdListener {
    private int mType;
    void AdListener(int type) {
        mType = type;
    }

    @Override
    public void didLoad() {
        //updateView();
        Log.i("MEDIATION-SAMPLE", "didLoad " + TMAdType.getString(mType));
        //mLogListAdapter.insert("didLoad " + TMAdType.getString(mType), 0);
    }

    @Override
    public void didFailToLoad(TMAdError error) {
        //updateView();

        String str = String.format(Locale.ENGLISH, "didFailToLoad %s: %d - %s", TMAdType.getString(mType), error.getErrorCode(), error.getErrorMessage());

        for (String key : error.getSubErrors().keySet()) {
            for (TMAdError value : error.getSubErrors().get(key)) {
                String subError = String.format(Locale.ENGLISH, "%s - %d: %s", key,  value.getErrorCode(), value.getErrorMessage());
                str = str.concat("\n ");
                str = str.concat(subError);
            }
        }

        Log.i("MEDIATION-SAMPLE", str);
        //mLogListAdapter.insert(str, 0);
    }

    @Override
    public void didRefresh() {
        Log.i("MEDIATION-SAMPLE", "didRefresh " + TMAdType.getString(mType));
        //mLogListAdapter.insert("didRefresh " + TMAdType.getString(mType), 0);
    }

    @Override
    public void willDisplay() {
        Log.i("MEDIATION-SAMPLE", "willDisplay " + TMAdType.getString(mType));
        //mLogListAdapter.insert("willDisplay " + TMAdType.getString(mType), 0);
    }

    @Override
    public void didDisplay() {
        Log.i("MEDIATION-SAMPLE", "didDisplay " + TMAdType.getString(mType));
        //mLogListAdapter.insert("didDisplay " + TMAdType.getString(mType), 0);
    }

    @Override
    public void didClick() {
        Log.i("MEDIATION-SAMPLE", "didClick " + TMAdType.getString(mType));
        //mLogListAdapter.insert("didClick " + TMAdType.getString(mType), 0);
    }

    @Override
    public void didVerify(TDReward reward) {
        String str = String.format(Locale.ENGLISH, "didVerify %s: Reward name: %s. Value: %d. Valid: %b. Custom Json: %s", TMAdType.getString(mType), reward.getName(), reward.getValue(), reward.isValid(), reward.getCustom_json().toString());
        Log.i("MEDIATION-SAMPLE", str);
        //mLogListAdapter.insert(str, 0);
    }

    @Override
    public void didClose() {
        //updateView();
        Log.i("MEDIATION-SAMPLE", "didClose " + TMAdType.getString(mType));
        //mLogListAdapter.insert("didClose " + TMAdType.getString(mType), 0);
    }
}
*/
