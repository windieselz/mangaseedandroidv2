package com.fridaystu.thaicook.helpers;


import android.Manifest;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;

import com.fridaystu.thaicook.utils.Contextor;
import com.pixplicity.easyprefs.library.Prefs;

import android.util.Log;
import android.provider.Settings.Secure;

import androidx.core.app.ActivityCompat;

import java.util.Random;


/**
 * Created by PPTV on 7/11/16 AD.
 */
public class UserHelper {

    private static Context mContext;
    private static UserHelper instance;
    private String user_key = "user_data_v2";
    private String purchase_key = "user_purchase";
    private SharedPreferences pref;

    private UserHelper() {
        mContext = Contextor.getInstance().getContext();
        setRandomUid();
    }

    public void setmContext(Context mcontext) {
        mContext = mcontext;
    }

    public static boolean isFaceBookInstalled() {
        String targetPackage = "com.facebook.katana";
        PackageManager pm = mContext.getPackageManager();
        try {
            PackageInfo info = pm.getPackageInfo(targetPackage, PackageManager.GET_META_DATA);
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
        return true;
    }

    public void setRandomUid() {

        String randUid = random();
        Prefs.putString("random_uid", randUid);
    }

    public String getRandomUid() {
        return Prefs.getString("random_uid", random());
    }

    public void setUid(String uid) {
        Prefs.putString("uid", uid);
    }

    public String getUid() {
        return Prefs.getString("uid", "");
    }

    public String getAdType() {
        if (Prefs.contains("adtype")) {
            //Log.i("pref", "get ad type : "+ Prefs.getString("adtype", "ad_default"));

            return Prefs.getString("adtype", "tapdaq");
        }

        return "tapdaq";
    }

    public void setAdsGap(String type, Integer sec) {
        Prefs.putInt(type, sec);
    }

    public int getAdsGap(String type) {
        return Prefs.getInt(type, 10);
    }

    public void setAdsLoadedTimestamp(String type) {
        Long ts = System.currentTimeMillis() / 1000;
        Prefs.putLong("ads_loaded_" + type, ts);
        Prefs.putLong("ads_loaded_global", ts);
    }

    public long getAdsLoadedTimestamp(String type) {
        return Prefs.getLong("ads_loaded_" + type, 0);
    }

    public void setAdType(String adType) {
        if (adType.equals("facebook") && isFaceBookInstalled()) {
            Prefs.putString("adtype", adType);
        } else {
            Prefs.putString("adtype", adType);
        }
    }

    public void setLastestCheckDevice() {
//		SharedPreferences.Editor editor = pref.edit();
        Long ts = System.currentTimeMillis() / 1000;
//		editor.putLong("lastest_check_available", ts);
//		editor.apply();
        Prefs.putLong("lastest_check_available", ts);
    }

    public boolean isTimeToCheckDevice() {
        if (Prefs.contains("lastest_check_available")) {
            //Long lastest =  pref.getLong("lastest_check_available", 0);
            Long lastest = Prefs.getLong("lastest_check_available", 0);
            Long ts = System.currentTimeMillis() / 1000;
            Log.d("Lastest check", lastest.toString() + " / " + ts.toString());
            if (ts - lastest < 10800) {
                return false;
            }
        }

        return true;
    }

    public static UserHelper getInstance() {
        if (instance == null) instance = new UserHelper();
        return instance;
    }

    public String getMacAddr() {
        String address = "";

        try {
            WifiManager manager = (WifiManager) mContext.getSystemService(Context.WIFI_SERVICE);
            WifiInfo info = manager.getConnectionInfo();
            if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return "";
            }
            address = info.getMacAddress();
        } catch (Exception e) {

        }
        return address;
    }

    public String getUDID() {
        try {
            return Secure.getString(mContext.getContentResolver(), Secure.ANDROID_ID);
        } catch (Exception e) {

        }

        String uid = getUid();
        if (!uid.equals("")) {
            return uid;
        }


        return getRandomUid();
    }

    public static String random() {
        Random generator = new Random();
        StringBuilder randomStringBuilder = new StringBuilder();
        int randomLength = generator.nextInt(96);
        char tempChar;
        for (int i = 0; i < randomLength; i++) {
            tempChar = (char) (generator.nextInt(96) + 32);
            randomStringBuilder.append(tempChar);
        }
        return randomStringBuilder.toString();
    }

    public boolean showAds() {
        return true;
//        Random random = new Random();
//        return random.nextBoolean();
    }

    public boolean isPurchase() {
        if (Prefs.contains(purchase_key)) return Prefs.getBoolean(purchase_key, false);
        return false;
    }

    public void setPurchase() {
        //SharedPreferences.Editor editor = pref.edit();
        Prefs.putBoolean(purchase_key, true);
        //editor.apply();
    }

    public void setUnPurchase() {
        //SharedPreferences.Editor editor = pref.edit();
        Prefs.putBoolean(purchase_key, false);
        //editor.apply();
    }

    public void setEmail(String email) {
        //SharedPreferences.Editor editor = pref.edit();
        Prefs.putString("tempemail", email);
        //editor.apply();
    }

    public String getEmail() {
        if (Prefs.contains("tempemail")) return Prefs.getString("tempemail", "");
        return "";
    }


    public void setReadEvent(String enable) {
        boolean readEnable = false;
        if (enable.equals("1")) {
            readEnable = true;
        }
        Prefs.putBoolean("read_enable", readEnable);

    }

    public boolean getReadEvent() {
        return Prefs.getBoolean("read_enable", false);
    }

    public void setViewEvent(String enable) {
        boolean viewEnable = false;
        if (enable.equals("1")) {
            viewEnable = true;
        }
        Prefs.putBoolean("view_enable", viewEnable);

    }

    public boolean getViewEvent() {
        return Prefs.getBoolean("view_enable", false);
    }


}

